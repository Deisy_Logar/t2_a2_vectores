

/*
		Ejercicio 1.
		
			El �rea de servicios escolares del ITSJ desea obtener los promedios por alumno en cada evaluaci�n parcial
			(4 evaluaciones parciales), si por cada alumno se tiene almacenados en 4 vectores diferentes las 6 calificaciones
			de cada materia, ejemplo:
			
			Calificaciones parciales 1 = {60, 89, 90, 100, 0, 56}
			Calificaciones parciales 2 = {....}
			
			Obtener:
				1. Promedio por parcial
				2. Promedio por materia
				3. Promedio general
			
*/



import scala.collection.mutable.ArrayBuffer
import java.text.DecimalFormat


object Vectores {
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=* PROMEDIOS *=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*")
    
    val calParciales1 = ArrayBuffer[Double]()
    val calParciales2 = ArrayBuffer[Double]()
    val calParciales3 = ArrayBuffer[Double]()
    val calParciales4 = ArrayBuffer[Double]()
    
    calParciales1 += (76, 72, 83, 91, 60, 65)
    calParciales2 += (55, 93, 56, 94, 100, 97)
    calParciales3 += (82, 83, 100, 72, 52, 82)
    calParciales4 += (26, 36, 55, 90, 83, 99)
    
    println("\n\n --------------- Calificaciones Parciales 1 --------------- \n")
    
    for(e <- calParciales1)
      print(e + ",  ")
    
    var sumCalParcial1: Double = 0.0
    var promParcial1: Double = 0.0
    
    for(x <- calParciales1) {
      sumCalParcial1 += x
    }
    
    promParcial1 = sumCalParcial1 / 6
    
    println("\n\n Promedio por parcial = " + promParcial1)
    
    println("\n\n --------------- Calificaciones Parciales 2 --------------- \n")
    
    for(e <- calParciales2)
      print(e + ",  ")
    
    var sumCalParcial2: Double = 0.0
    var promParcial2: Double = 0.0
    
    for(x <- calParciales2) {
      sumCalParcial2 += x
    }
    
    promParcial2 = sumCalParcial2 / 6
    
    println("\n\n Promedio por parcial = " + promParcial2)
    
    println("\n\n --------------- Calificaciones Parciales 3 --------------- \n")
    
    for(e <- calParciales3)
      print(e + ",  ")
    
    var sumCalParcial3: Double = 0.0
    var promParcial3: Double = 0.0
    
    for(x <- calParciales3) {
      sumCalParcial3 += x
    }
    
    promParcial3 = sumCalParcial3 / 6
    
    println("\n\n Promedio por parcial = " + promParcial3)
    
    println("\n\n --------------- Calificaciones Parciales 4 --------------- \n")
    
    for(e <- calParciales4)
      print(e + ",  ")
    
    var sumCalParcial4: Double = 0.0
    var promParcial4: Double = 0.0
    
    for(x <- calParciales4) {
      sumCalParcial4 += x
    }
    
    promParcial4 = sumCalParcial4 / 6
    
    println("\n\n Promedio por parcial = " + promParcial4)
    
    println("\n\n\n\n ------------------------------ PROMEDIO POR MATERIA ------------------------------")
    
    var sumMateria1: Double = 0.0
    var sumMateria2: Double = 0.0
    var sumMateria3: Double = 0.0
    var sumMateria4: Double = 0.0
    var sumMateria5: Double = 0.0
    var sumMateria6: Double = 0.0
    
    sumMateria1 = calParciales1(0) + calParciales2(0) + calParciales3(0) + calParciales4(0)
    sumMateria2 = calParciales1(1) + calParciales2(1) + calParciales3(1) + calParciales4(1)
    sumMateria3 = calParciales1(2) + calParciales2(2) + calParciales3(2) + calParciales4(2)
    sumMateria4 = calParciales1(3) + calParciales2(3) + calParciales3(3) + calParciales4(3)
    sumMateria5 = calParciales1(4) + calParciales2(4) + calParciales3(4) + calParciales4(4)
    sumMateria6 = calParciales1(5) + calParciales2(5) + calParciales3(5) + calParciales4(5)
    
    var promMateria1: Double = 0.0
    var promMateria2: Double = 0.0
    var promMateria3: Double = 0.0
    var promMateria4: Double = 0.0
    var promMateria5: Double = 0.0
    var promMateria6: Double = 0.0
    
    promMateria1 = sumMateria1 / 4
    promMateria2 = sumMateria2 / 4
    promMateria3 = sumMateria3 / 4
    promMateria4 = sumMateria4 / 4
    promMateria5 = sumMateria5 / 4
    promMateria6 = sumMateria6 / 4
    
    println("\n Promedio Materia 1 = " + promMateria1)
    println("\n Promedio Materia 2 = " + promMateria2)
    println("\n Promedio Materia 3 = " + promMateria3)
    println("\n Promedio Materia 4 = " + promMateria4)
    println("\n Promedio Materia 5 = " + promMateria5)
    println("\n Promedio Materia 6 = " + promMateria6)
    
    println("\n\n\n\n ------------------------------ PROMEDIO GENERAL ------------------------------")
    
    var sumPromMaterias: Double = 0.0
    var promGeneral: Double = 0.0
    
    sumPromMaterias = promMateria1 + promMateria2 + promMateria3 + promMateria4 + promMateria5 + promMateria6
    
    promGeneral = sumPromMaterias / 6
    
    println("\n Promedio General = " + promGeneral)
    
  }
}