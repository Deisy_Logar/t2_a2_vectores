

/*
		Ejercicio 2.
		
			Una empresa encargada de las detonaciones de explosivos en una mina desea saber cu�les horas
			del d�a son mejores para realizar detonaciones, para ello, necesitan saber las temperaturas
			que sean exactamente igual a 25�C. Dicha empresa almacena los datos de un sensor de temperatura
			en un vector (o arreglo) de donde desean obtener la informaci�n para mostrar las horas en las
			que se da una temperatura exacta de 25 grados.
			
			La informaci�n se guarda en un Vector (Array) Unidimensional, indicando la hora del d�a y la
			temeperatura, de la siguuiente manera:
			
			-------------------------------------------------------------------------------------------------------
			|  0  |      34      |  1  |      11     |  2  | 50 | 3 | 12 | ... | ... | 23 | 3 |  23  |      5     |
			-------------------------------------------------------------------------------------------------------
			 Hora		Temperatura		Hora	 Temperatura																						Hora	Temperatura
			 
			
			1. Llenar las posiciones PARES del arreglo con n�meros que indican las horas del d�a
			   consecutivamene, desde 0 hasta 23.
			
			2. Llenar las posiciones IMPARES del arreglo con temperaturas ALEATORIAS entre 0 y 50.
			
			3. Mostrar las temperaturas que sean exactamente igual a 25 y la hora en que se registraron.
			
			4. Guardar las temperaturas que sean exactamente igual a 25 y la HORA en que se registraron
				 en OTRO VECTOR y posteriormente obtener el promedio de dichas horas.
			
*/

import scala.collection.mutable.ArrayBuffer
import scala.math.random
import scala.util.Random


object Ejercicio02_Empresa {
  
  def obtenerHoras(vector: Array[Int]): Array[Int] = {
    var aux: Int = 0
    var numAleatorio: Int = 0
    
    for(x <- 0 to 47) {
      
      numAleatorio = (Math.random() * 50 +1).toInt
      
      if(x % 2 == 0) {
        vector(x) = aux
        aux += 1
        
      } else {
        vector(x) = numAleatorio
      }
    }
    
    vector
  } // Obtener Horas y Temperaturas
  
  def mostrarVector(vector: Array[Int]): Unit = {
    print("\n\t")
    
    for(x <- vector) {
      print(x + ", ")
    }
  } // Mostrar Vector
  
  def mostrarTemperaturas(vector: Array[Int]): Unit = {
    
    print("\n\n")
    
    for(x <- vector) {
      
      if (x % 2 != 0) {
        
        if(vector(x) == 25) {
          println("Temperatura: " + vector(x) + " ---> Hora: " + vector(x - 1))
        }
      }
      
    }
    
  }// Mostrar temperaturas iguales a 25
  
  def main(args: Array[String]): Unit = {
    
    println("\n\t\t*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=* EMPRESA *=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*")
    
    val horasTemperaturas = new Array[Int](48)
    
    val horasTemps = obtenerHoras(horasTemperaturas).clone()
    
    mostrarVector(horasTemps)
    
    mostrarTemperaturas(horasTemps)
  }
}